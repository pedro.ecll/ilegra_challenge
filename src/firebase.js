import Firebase from 'firebase'

const firebaseApp = Firebase.initializeApp({
  apiKey: "AIzaSyBA8Kq3rNhePQuTWLUk5eFvWk7MeV3UrMc",
  authDomain: "ilegra-challenge.firebaseapp.com",
  databaseURL: "https://ilegra-challenge.firebaseio.com",
  projectId: "ilegra-challenge",
  storageBucket: "ilegra-challenge.appspot.com",
  messagingSenderId: "866498697246"
});

export const db = firebaseApp.database();
export const auth = firebaseApp.auth();
export const storage = firebaseApp.storage();
