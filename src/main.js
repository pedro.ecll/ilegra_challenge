import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueFire from 'vuefire'

import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify)
Vue.use(VueFire)

new Vue({
  el: '#app',
  render: h => h(App)
})
